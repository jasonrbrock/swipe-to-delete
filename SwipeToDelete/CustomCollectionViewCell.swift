//
//  CustomCollectionViewCell.swift
//  SwipeToDelete
//
//  Created by Anish on 11/2/16.
//  Copyright © 2016 Anish. All rights reserved.
//

import Foundation
import UIKit
import SBAutoLayout

protocol SendCommandToVC{
    func deleteThisCell()
}

class CustomCollectionViewCell: UICollectionViewCell,UIGestureRecognizerDelegate {
    
    var swipeView: UIView = {
        let theBackground = UIView(frame:CGRect(x:0, y:0, width:60, height:60))
        theBackground.backgroundColor = UIColor.red
        theBackground.translatesAutoresizingMaskIntoConstraints = false
        return theBackground
    }()
    
    var customContentView: UIView = {
        let theBackground = UIView(frame:CGRect(x:0, y:0, width:60, height:60))
        theBackground.backgroundColor = UIColor.yellow
        theBackground.translatesAutoresizingMaskIntoConstraints = false
        return theBackground
    }()

    var customTextLbl: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
        label.center = CGPoint(x: 160, y: 285)
        label.textAlignment = .center
        label.text = "I'am a test label"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var deleteLabel: UITextView = {
        let labelView = UITextView()
        labelView.translatesAutoresizingMaskIntoConstraints = false
        labelView.isUserInteractionEnabled = false
        labelView.font = UIFont(name: "Pangram-Bold", size: 13.0)
        labelView.text = "DELETE"
        labelView.textAlignment = .center
        labelView.textContainerInset = UIEdgeInsetsMake(2, 4, 2, 2)
        labelView.textColor = UIColor.white
        labelView.backgroundColor = UIColor.clear
        return labelView
    }()
    
    var deleteButton: UIButton = {
        let theButton = UIButton()
        theButton.frame = CGRect(x:0, y:0, width:100, height:40)
        theButton.backgroundColor = UIColor.clear
        return theButton
    }()
    
    // ---------------------------------
    
    var delegate:SendCommandToVC?
    
    var startPoint = CGPoint()
    var startingRightLayoutConstraintConstant = CGFloat()
    
    var contentViewLeftConstraint: NSLayoutConstraint!
    var contentViewRightConstraint: NSLayoutConstraint!
    
    func unpinThisUnit(_ sender: AnyObject) {
        if let temp = delegate {
            temp.deleteThisCell()
        }else{
            print("you forgot to set the delegate")
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        deleteButton.addTarget(self, action:#selector(unpinThisUnit(_:)), for: .touchUpInside)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        
        addSubview(swipeView)
        addSubview(customContentView)
        customContentView.addSubview(customTextLbl)
        
        swipeView.addSubview(deleteButton)
        deleteButton.pinToSuperviewEdges()
        
        swipeView.addSubview(deleteLabel)
        deleteLabel.pinToSuperviewCenter()
        deleteLabel.pinToSuperviewLeading(margin: 5)
        deleteLabel.pinToSuperviewTrailing(margin: 5)
        deleteLabel.pinHeight(20)
        
        swipeView.pinToSuperviewRight(margin: 0)
        swipeView.pinToSuperviewTop(margin: 10)
        swipeView.pinToSuperviewBottom(margin: 10)
        swipeView.pinWidth(150)
        
        customContentView.pinToSuperviewLeading(margin: 10, priority: 1000)
        customContentView.pinToSuperviewTop(margin: 10)
        customContentView.pinToSuperviewBottom(margin: 10)
        customContentView.pinToSuperviewTrailing(margin: 150, priority: 1000)
        
        contentViewLeftConstraint = customContentView.pinToSuperviewLeading(margin: 10, priority: 1000)
        contentViewRightConstraint = customContentView.pinToSuperviewTrailing(margin: 10, priority: 1000)
        
        customTextLbl.pinToSuperviewCenter()
        
    }
    
}
