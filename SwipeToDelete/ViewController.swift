//
//  ViewController.swift
//  SwipeToDelete
//
//  Created by Anish on 11/2/16.
//  Copyright © 2016 Anish. All rights reserved.
//

import Foundation
import UIKit
import SBAutoLayout

class ViewController: UIViewController, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate {
    
    var collectionView: UICollectionView!
    
    // ----------------------------------------
    // SwipeTodelete
    var panGesture = UIPanGestureRecognizer()
    let kBounceValue:CGFloat = 0
    var swipeActiveCell:CustomCollectionViewCell?
    var numberOfCell = 30
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let width = view.bounds.size.width - 6
        let collectionViewLayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewLayout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10)
        collectionViewLayout.itemSize = CGSize(width: width/3, height: view.bounds.size.height - 20 )
        collectionViewLayout.minimumInteritemSpacing = 0
        collectionViewLayout.minimumLineSpacing = 0
        
        collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: collectionViewLayout)
        collectionView?.backgroundColor = UIColor.black
        collectionView?.register(CustomCollectionViewCell.self, forCellWithReuseIdentifier: "cellId")
        collectionView?.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        collectionView?.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.translatesAutoresizingMaskIntoConstraints = false
        
        self.panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.panThisCell))
        panGesture.delegate = self
        self.collectionView.addGestureRecognizer(panGesture)
        
        setupUI(theCollectionView: collectionView!)
        
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(numberOfCell)
        return numberOfCell
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! CustomCollectionViewCell
        cell.clipsToBounds = true
        cell.customTextLbl.text = "Swipe To Delete"
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.view.frame.size.width)
        let height = (self.view.frame.size.height / 2.75)
        return CGSize(width: width, height: height)
    }
    
    func setupUI(theCollectionView: UICollectionView) {
        view.addSubview(theCollectionView)
        theCollectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView?.pinToSuperviewEdges(top: 40, bottom: 70, leading: 7, trailing: 7)
        
    }
    
}


    




